/**
 * Defines the levels as an enum with constants used as the corresponding values which allows lookup in the
 * level values table
 */
export declare enum LogLevel {
    BASE = "BASE",
    TRACE = "TRACE",
    DEBUG = "DEBUG",
    INFO = "INFO",
    WARN = "WARN",
    ERROR = "ERROR",
    FATAL = "FATAL"
}
/**
 * The internal config which controls coloring, minimum logging levels and stores the set of interceptors for the
 * logging function
 */
export declare const config: any;
/**
 * Sets the coloring function for a given log level. The provided function should take a string (the value logged) and
 * return a string which will actually be logged to the console with any effects applied
 * @param level {LogLevel} the log level for the given function to be applied to
 * @param executor {(data: string) => string} the function which applies effects to the string
 */
export declare function setColorFunction(level: LogLevel, executor: (data: string) => string): void;
/**
 * Adds a handler function which will be called on every log with a data object containing all the data about the log.
 * THe handler will still be called if a message has been rejected. If the function returns false then the message will
 * be rejected and not logged to the console.
 * The handler takes one data object which will contain the following keys:
 * <table>
 *     <tr>
 *         <th>time</th>
 *         <td>The time at which the log was submitted in the format YYYY-MM-DD HH:mm:ss</td>
 *     </tr>
 *     <tr>
 *         <th>caller</th>
 *         <td>The caller of the log function containing values method, file, line, character, context and stack</td>
 *     </tr>
 *     <tr>
 *         <th>message</th>
 *         <td>The message that was actually logged to the console</td>
 *     </tr>
 *     <tr>
 *         <th>color</th>
 *         <td>The coloring function that will being applied to the message before logging. Will be null if coloring is
 *         skipped</td>
 *     </tr>
 *     <tr>
 *         <th>level</th>
 *         <td>The level at which the message will be logged</td>
 *     </tr>
 *     <tr>
 *         <th>logFunction</th>
 *         <td>The function through which the value will be logged</td>
 *     </tr>
 *     <tr>
 *         <th>rejected</th>
 *         <td>If the message has been rejected <i>Note: this is not a guarantee that it will be logged if it is true.
 *         As handlers can reject messages on their own, if your handler is called before one that rejects the message,
 *         you will see it as not being rejected and any after it will see it as being rejected. Position matters in
 *         handlers</i></td>
 *     </tr>
 * </table>
 * @param handler {(data: object) => boolean | undefined} the handler
 */
export declare function addHandler(handler: (data: object) => boolean | undefined): void;
/**
 * Sets the minimum level that will be output. The outputs will include the given level
 * @param level {LogLevel} the minimum level to output
 */
export declare function setMinimumLevel(level: LogLevel): void;
/**
 * Sets the config value {@link config.colorsDisabled} which will skip all coloring functions
 */
export declare function disableColors(): void;
/**
 * Sets the config value {@link config.colorsDisabled} which will prevent skipping of coloring functions
 */
export declare function enableColors(): void;
/**
 * Sets the given key to the given value in the config. This should not technically be needed but is there for future
 * proofing. All config values should be able to be updated via {@link setMinimumLevel}, {@link setColorFunction},
 * {@link addHandler}, {@link disableColors} and {@link enableColors}
 * @param key {string} the key to update in the config (keys that descend levels should use dots to represent levels eg colors.INFO)
 * @param data {any} the value the key should be updated to
 */
export declare function configure(key: string, data: any): void;
/**
 * Logs the message and data through {@link console.log} using the coloring function in {@link config} which can be
 * updated by {@link setColorFunction} via {@link output}
 * @param message the message to log the console
 * @param data the message to log alongside this message
 */
export declare function log(message: string, ...data: any[]): void;
/**
 * Logs the message and data through {@link console.debug} using the coloring function in {@link config} which can be
 * updated by {@link setColorFunction} via {@link output}
 * @param message the message to log the console
 * @param data the message to log alongside this message
 */
export declare function trace(message: string, ...data: any[]): void;
/**
 * Logs the message and data through {@link console.debug} using the coloring function in {@link config} which can be
 * updated by {@link setColorFunction} via {@link output}
 * @param message the message to log the console
 * @param data the message to log alongside this message
 */
export declare function debug(message: string, ...data: any[]): void;
/**
 * Logs the message and data through {@link console.log} using the coloring function in {@link config} which can be
 * updated by {@link setColorFunction} via {@link output}
 * @param message the message to log the console
 * @param data the message to log alongside this message
 */
export declare function info(message: string, ...data: any[]): void;
/**
 * Logs the message and data through {@link console.warn} using the coloring function in {@link config} which can be
 * updated by {@link setColorFunction} via {@link output}
 * @param message the message to log the console
 * @param data the message to log alongside this message
 */
export declare function warn(message: string, ...data: any[]): void;
/**
 * Logs the message and data through {@link console.error} using the coloring function in {@link config} which can be
 * updated by {@link setColorFunction} via {@link output}
 * @param message the message to log the console
 * @param data the message to log alongside this message
 */
export declare function error(message: string, ...data: any[]): void;
/**
 * Logs the message and data through {@link console.error} using the coloring function in {@link config} which can be
 * updated by {@link setColorFunction} via {@link output}
 * @param message the message to log the console
 * @param data the message to log alongside this message
 */
export declare function fatal(message: string, ...data: any[]): void;
/**
 * A single object that contains all the logging functions, useful for passing around when required and allows importing
 * a single object
 */
export declare const Logs: {
    fatal: typeof fatal;
    error: typeof error;
    warn: typeof warn;
    info: typeof info;
    debug: typeof debug;
    trace: typeof trace;
    log: typeof log;
};
