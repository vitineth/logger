"use strict";
exports.__esModule = true;
var colors = require("colors");
var path_1 = require("path");
var util = require("util");
var flattedStringify = require('flatted/cjs').stringify;
/**
 * Defines the levels as an enum with constants used as the corresponding values which allows lookup in the
 * level values table
 */
var LogLevel;
(function (LogLevel) {
    LogLevel["BASE"] = "BASE";
    LogLevel["TRACE"] = "TRACE";
    LogLevel["DEBUG"] = "DEBUG";
    LogLevel["INFO"] = "INFO";
    LogLevel["WARN"] = "WARN";
    LogLevel["ERROR"] = "ERROR";
    LogLevel["FATAL"] = "FATAL";
})(LogLevel = exports.LogLevel || (exports.LogLevel = {}));
/**
 * Relates the string log levels to their numeric values which allows direct comparison of priority
 */
var levelMappings = {
    BASE: 0,
    TRACE: 2,
    DEBUG: 3,
    INFO: 4,
    WARN: 5,
    ERROR: 6,
    FATAL: 7
};
/**
 * The internal config which controls coloring, minimum logging levels and stores the set of interceptors for the
 * logging function
 */
exports.config = {
    minLevel: LogLevel.BASE,
    colorsDisabled: false,
    colors: {
        BASE: function (e) { return e; },
        LOG: function (e) { return e; },
        TRACE: colors.green,
        DEBUG: colors.magenta,
        INFO: function (e) { return e; },
        WARN: colors.yellow,
        ERROR: colors.red,
        FATAL: colors.bgRed
    },
    handlers: []
};
/**
 * Sets the coloring function for a given log level. The provided function should take a string (the value logged) and
 * return a string which will actually be logged to the console with any effects applied
 * @param level {LogLevel} the log level for the given function to be applied to
 * @param executor {(data: string) => string} the function which applies effects to the string
 */
function setColorFunction(level, executor) {
    exports.config.colors[level] = executor;
}
exports.setColorFunction = setColorFunction;
/**
 * Adds a handler function which will be called on every log with a data object containing all the data about the log.
 * THe handler will still be called if a message has been rejected. If the function returns false then the message will
 * be rejected and not logged to the console.
 * The handler takes one data object which will contain the following keys:
 * <table>
 *     <tr>
 *         <th>time</th>
 *         <td>The time at which the log was submitted in the format YYYY-MM-DD HH:mm:ss</td>
 *     </tr>
 *     <tr>
 *         <th>caller</th>
 *         <td>The caller of the log function containing values method, file, line, character, context and stack</td>
 *     </tr>
 *     <tr>
 *         <th>message</th>
 *         <td>The message that was actually logged to the console</td>
 *     </tr>
 *     <tr>
 *         <th>color</th>
 *         <td>The coloring function that will being applied to the message before logging. Will be null if coloring is
 *         skipped</td>
 *     </tr>
 *     <tr>
 *         <th>level</th>
 *         <td>The level at which the message will be logged</td>
 *     </tr>
 *     <tr>
 *         <th>logFunction</th>
 *         <td>The function through which the value will be logged</td>
 *     </tr>
 *     <tr>
 *         <th>rejected</th>
 *         <td>If the message has been rejected <i>Note: this is not a guarantee that it will be logged if it is true.
 *         As handlers can reject messages on their own, if your handler is called before one that rejects the message,
 *         you will see it as not being rejected and any after it will see it as being rejected. Position matters in
 *         handlers</i></td>
 *     </tr>
 * </table>
 * @param handler {(data: object) => boolean | undefined} the handler
 */
function addHandler(handler) {
    exports.config.handlers.push(handler);
}
exports.addHandler = addHandler;
/**
 * Sets the minimum level that will be output. The outputs will include the given level
 * @param level {LogLevel} the minimum level to output
 */
function setMinimumLevel(level) {
    exports.config.minLevel = level;
}
exports.setMinimumLevel = setMinimumLevel;
/**
 * Sets the config value {@link config.colorsDisabled} which will skip all coloring functions
 */
function disableColors() {
    exports.config.colorsDisabled = true;
}
exports.disableColors = disableColors;
/**
 * Sets the config value {@link config.colorsDisabled} which will prevent skipping of coloring functions
 */
function enableColors() {
    exports.config.colorsDisabled = false;
}
exports.enableColors = enableColors;
/**
 * Sets the given key to the given value in the config. This should not technically be needed but is there for future
 * proofing. All config values should be able to be updated via {@link setMinimumLevel}, {@link setColorFunction},
 * {@link addHandler}, {@link disableColors} and {@link enableColors}
 * @param key {string} the key to update in the config (keys that descend levels should use dots to represent levels eg colors.INFO)
 * @param data {any} the value the key should be updated to
 */
function configure(key, data) {
    var active = exports.config;
    var segments = key.split('.');
    var last = segments[segments.length - 1];
    for (var i = 0; i < segments.length - 1; i++) {
        var element = segments[i];
        if (active.hasOwnProperty(element))
            active = active[element];
        else
            throw new Error('Invalid config key');
    }
    active[last] = data;
}
exports.configure = configure;
/**
 * Pads the given string to the given length using the provided fill value (defaults to '0'). If the
 * given date is longer than or equal to the length it will be returned unchanged
 * @param data {string} the data to pad
 * @param length {number} the minimum length of the string
 * @param [fill='0'] {string} the character to fill the string with
 */
function padString(data, length, fill) {
    if (fill === void 0) { fill = '0'; }
    if (data.length >= length)
        return data;
    var dataClone = data;
    while (dataClone.length < length)
        dataClone += fill;
    return dataClone;
}
/**
 * Pads the given number to the given length using {@link padString}
 * @param data {number} the data to pad
 * @param length {number} the minimum length of the string
 * @param [fill='0'] {string} the character to fill the string with
 */
function padNumber(data, length, fill) {
    if (fill === void 0) { fill = '0'; }
    return padString(String(data), length, fill);
}
/**
 * Returns the current date and time (as fetched from a new Date instance) in the form
 * YYYY-MM-DD hh:mm:ss
 */
function getFormattedDateTime() {
    var date = new Date();
    var year = padNumber(date.getFullYear(), 4);
    var month = padNumber(date.getMonth() + 1, 2);
    var day = padNumber(date.getDate(), 2);
    var hour = padNumber(date.getHours(), 2);
    var minute = padNumber(date.getMinutes(), 2);
    var second = padNumber(date.getSeconds(), 2);
    return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
}
/**
 * Extracts the caller of the given depth from a new error stack. An error will be thrown if the
 * stack cannot be fetched (returns an undefined value), if there are not enough stack elements
 * to fetch the given depth or if the stack string is not parsable.
 * @param [depth=3] {number} the depth in the stack to fetch
 */
function extractCallerFromError(depth) {
    if (depth === void 0) { depth = 3; }
    var stack = new Error().stack;
    if (stack === undefined)
        throw new Error('failed to extract stack from a new error instance');
    var stackLines = stack.split('\n').slice(1);
    // Preprocess the stack lines
    stackLines = stackLines.map(function (line) { return line.trim(); });
    // Extract the caller for the given depth
    if (stackLines.length <= depth)
        throw new Error('call stack is too shallow to log');
    var target = stackLines[depth];
    var extractor = /^at (.+?) \((.+?):([0-9]+):([0-9]+)\)$/g;
    var elements = extractor.exec(target);
    var method;
    var file;
    var line;
    var character;
    if (elements === null) {
        extractor = /^at (.+?):([0-9]+):([0-9]+)$/g;
        elements = extractor.exec(target);
        if (elements === null)
            throw new Error('target element does not match a valid stack string: ' + target);
        method = '<no method>';
        file = elements[1];
        line = elements[2];
        character = elements[3];
    }
    else {
        method = elements[1];
        file = elements[2];
        line = elements[3];
        character = elements[4];
    }
    var pathSegments = file.split(path_1.sep);
    var context = pathSegments[0];
    if (pathSegments.length >= 2) {
        context = pathSegments.slice(pathSegments.length - 2, pathSegments.length).join(path_1.sep);
    }
    return {
        method: method,
        file: file,
        line: line,
        character: character,
        context: context,
        stack: target
    };
}
/**
 * The function which logs the value to console (if it is allowed). It will get the log info from
 * {@link extractCallerFromError} and {@link getFormattedDateTime}. All handlers will be called with the details of the
 * log. If any return false or the log level is too low (beneath {@link config.minLevel} as set via
 * {@link setMinimumLevel}.
 * @param level {LogLevel} the level at which this message should be logged
 * @param message {string} the message to be logged
 * @param logFunction {Function} the function through which the value should be logged
 * @param color {(e: string) => string} the coloring function
 */
function output(level, message, logFunction, color) {
    if (logFunction === void 0) { logFunction = console.log; }
    if (color === void 0) { color = function (e) { return e; }; }
    var reject = levelMappings[level.toString()] < levelMappings[exports.config.minLevel];
    var caller = extractCallerFromError();
    var time = getFormattedDateTime();
    if (exports.config.hasOwnProperty('handlers')) {
        for (var _i = 0, _a = exports.config.handlers; _i < _a.length; _i++) {
            var handler = _a[_i];
            if (typeof (handler) === 'function') {
                var result = handler({
                    time: time,
                    caller: caller,
                    message: message,
                    color: exports.config.colorsDisabled ? null : color,
                    level: level,
                    logFunction: logFunction,
                    rejected: reject
                });
                if (result === false)
                    reject = true;
            }
        }
    }
    if (reject)
        return;
    if (exports.config.colorsDisabled) {
        logFunction("[" + time + "][" + caller.context + ":" + caller.line + " " + caller.method + "][" + level + "] " + message);
    }
    else {
        logFunction(color("[" + time + "][" + caller.context + ":" + caller.line + " " + caller.method + "][" + level + "] " + message));
    }
}
/**
 * Converts the given value to a string using {@link flattedStringify} but with exceptions for:
 * <ul>
 *     <li>undefined</li>
 *     <li>null</li>
 *     <li>Infinity / -Infinity</li>
 *     <li>NaN</li>
 *     <li>Error</li>
 * </ul>
 * @param value {any} the value to convert to string
 */
function stringify(value) {
    var manualValues = [undefined, null, Infinity, -Infinity, NaN];
    for (var _i = 0, manualValues_1 = manualValues; _i < manualValues_1.length; _i++) {
        var test = manualValues_1[_i];
        if (value === test)
            return String(test);
    }
    if (value === -0)
        return '-0';
    return util.inspect(value, true, null, true);
}
/**
 * Logs the message and data through {@link console.log} using the coloring function in {@link config} which can be
 * updated by {@link setColorFunction} via {@link output}
 * @param message the message to log the console
 * @param data the message to log alongside this message
 */
function log(message) {
    var data = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        data[_i - 1] = arguments[_i];
    }
    output(LogLevel.BASE, message, console.log, exports.config.colors.LOG);
    for (var _a = 0, data_1 = data; _a < data_1.length; _a++) {
        var value = data_1[_a];
        if (value === undefined || value === null)
            output(LogLevel.BASE, " -> " + value, console.log, exports.config.colors.LOG);
        else
            output(LogLevel.BASE, " -> " + stringify(value), console.log, exports.config.colors.LOG);
    }
}
exports.log = log;
/**
 * Logs the message and data through {@link console.debug} using the coloring function in {@link config} which can be
 * updated by {@link setColorFunction} via {@link output}
 * @param message the message to log the console
 * @param data the message to log alongside this message
 */
function trace(message) {
    var data = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        data[_i - 1] = arguments[_i];
    }
    output(LogLevel.TRACE, message, console.debug, exports.config.colors.TRACE);
    for (var _a = 0, data_2 = data; _a < data_2.length; _a++) {
        var value = data_2[_a];
        if (value === undefined || value === null)
            output(LogLevel.TRACE, " -> " + value, console.log, exports.config.colors.TRACE);
        else
            output(LogLevel.TRACE, " -> " + stringify(value), console.log, exports.config.colors.TRACE);
    }
}
exports.trace = trace;
/**
 * Logs the message and data through {@link console.debug} using the coloring function in {@link config} which can be
 * updated by {@link setColorFunction} via {@link output}
 * @param message the message to log the console
 * @param data the message to log alongside this message
 */
function debug(message) {
    var data = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        data[_i - 1] = arguments[_i];
    }
    output(LogLevel.DEBUG, message, console.debug, exports.config.colors.DEBUG);
    for (var _a = 0, data_3 = data; _a < data_3.length; _a++) {
        var value = data_3[_a];
        if (value === undefined || value === null)
            output(LogLevel.DEBUG, " -> " + value, console.log, exports.config.colors.DEBUG);
        else
            output(LogLevel.DEBUG, " -> " + stringify(value), console.log, exports.config.colors.DEBUG);
    }
}
exports.debug = debug;
/**
 * Logs the message and data through {@link console.log} using the coloring function in {@link config} which can be
 * updated by {@link setColorFunction} via {@link output}
 * @param message the message to log the console
 * @param data the message to log alongside this message
 */
function info(message) {
    var data = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        data[_i - 1] = arguments[_i];
    }
    output(LogLevel.INFO, message, console.log, exports.config.colors.INFO);
    for (var _a = 0, data_4 = data; _a < data_4.length; _a++) {
        var value = data_4[_a];
        if (value === undefined || value === null)
            output(LogLevel.INFO, " -> " + value, console.log, exports.config.colors.INFO);
        else
            output(LogLevel.INFO, " -> " + stringify(value), console.log, exports.config.colors.INFO);
    }
}
exports.info = info;
/**
 * Logs the message and data through {@link console.warn} using the coloring function in {@link config} which can be
 * updated by {@link setColorFunction} via {@link output}
 * @param message the message to log the console
 * @param data the message to log alongside this message
 */
function warn(message) {
    var data = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        data[_i - 1] = arguments[_i];
    }
    output(LogLevel.WARN, message, console.warn, exports.config.colors.WARN);
    for (var _a = 0, data_5 = data; _a < data_5.length; _a++) {
        var value = data_5[_a];
        if (value === undefined || value === null)
            output(LogLevel.WARN, " -> " + value, console.warn, exports.config.colors.WARN);
        else
            output(LogLevel.WARN, " -> " + stringify(value), console.warn, exports.config.colors.WARN);
    }
}
exports.warn = warn;
/**
 * Logs the message and data through {@link console.error} using the coloring function in {@link config} which can be
 * updated by {@link setColorFunction} via {@link output}
 * @param message the message to log the console
 * @param data the message to log alongside this message
 */
function error(message) {
    var data = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        data[_i - 1] = arguments[_i];
    }
    output(LogLevel.ERROR, message, console.error, exports.config.colors.ERROR);
    for (var _a = 0, data_6 = data; _a < data_6.length; _a++) {
        var value = data_6[_a];
        if (value === undefined || value === null)
            output(LogLevel.ERROR, " -> " + value, console.error, exports.config.colors.ERROR);
        else
            output(LogLevel.ERROR, " -> " + stringify(value), console.error, exports.config.colors.ERROR);
    }
}
exports.error = error;
/**
 * Logs the message and data through {@link console.error} using the coloring function in {@link config} which can be
 * updated by {@link setColorFunction} via {@link output}
 * @param message the message to log the console
 * @param data the message to log alongside this message
 */
function fatal(message) {
    var data = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        data[_i - 1] = arguments[_i];
    }
    output(LogLevel.FATAL, message, console.error, exports.config.colors.FATAL);
    for (var _a = 0, data_7 = data; _a < data_7.length; _a++) {
        var value = data_7[_a];
        if (value === undefined || value === null)
            output(LogLevel.FATAL, " -> " + value, console.error, exports.config.colors.FATAL);
        else
            output(LogLevel.FATAL, " -> " + stringify(value), console.error, exports.config.colors.FATAL);
    }
}
exports.fatal = fatal;
/**
 * A single object that contains all the logging functions, useful for passing around when required and allows importing
 * a single object
 */
exports.Logs = {
    fatal: fatal,
    error: error,
    warn: warn,
    info: info,
    debug: debug,
    trace: trace,
    log: log
};
