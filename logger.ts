import * as colors from 'colors';
import { sep } from 'path';
import * as util from 'util';

const flattedStringify = require('flatted/cjs').stringify;

/**
 * Defines the levels as an enum with constants used as the corresponding values which allows lookup in the
 * level values table
 */

export enum LogLevel {
    BASE = 'BASE',
    TRACE = 'TRACE',
    DEBUG = 'DEBUG',
    INFO = 'INFO',
    WARN = 'WARN',
    ERROR = 'ERROR',
    FATAL = 'FATAL',
}

/**
 * Relates the string log levels to their numeric values which allows direct comparison of priority
 */
const levelMappings: { [key: string]: number } = {
    BASE: 0,
    TRACE: 2,
    DEBUG: 3,
    INFO: 4,
    WARN: 5,
    ERROR: 6,
    FATAL: 7,
};

/**
 * The internal config which controls coloring, minimum logging levels and stores the set of interceptors for the
 * logging function
 */
export const config: any = {
    minLevel: LogLevel.BASE,
    colorsDisabled: false,
    colors: {
        BASE: (e: string) => e,
        LOG: (e: string) => e,
        TRACE: colors.green,
        DEBUG: colors.magenta,
        INFO: (e: string) => e,
        WARN: colors.yellow,
        ERROR: colors.red,
        FATAL: colors.bgRed,
    },
    handlers: [],
};

/**
 * Sets the coloring function for a given log level. The provided function should take a string (the value logged) and
 * return a string which will actually be logged to the console with any effects applied
 * @param level {LogLevel} the log level for the given function to be applied to
 * @param executor {(data: string) => string} the function which applies effects to the string
 */
export function setColorFunction(level: LogLevel, executor: (data: string) => string) {
    config.colors[level] = executor;
}

/**
 * Adds a handler function which will be called on every log with a data object containing all the data about the log.
 * THe handler will still be called if a message has been rejected. If the function returns false then the message will
 * be rejected and not logged to the console.
 * The handler takes one data object which will contain the following keys:
 * <table>
 *     <tr>
 *         <th>time</th>
 *         <td>The time at which the log was submitted in the format YYYY-MM-DD HH:mm:ss</td>
 *     </tr>
 *     <tr>
 *         <th>caller</th>
 *         <td>The caller of the log function containing values method, file, line, character, context and stack</td>
 *     </tr>
 *     <tr>
 *         <th>message</th>
 *         <td>The message that was actually logged to the console</td>
 *     </tr>
 *     <tr>
 *         <th>color</th>
 *         <td>The coloring function that will being applied to the message before logging. Will be null if coloring is
 *         skipped</td>
 *     </tr>
 *     <tr>
 *         <th>level</th>
 *         <td>The level at which the message will be logged</td>
 *     </tr>
 *     <tr>
 *         <th>logFunction</th>
 *         <td>The function through which the value will be logged</td>
 *     </tr>
 *     <tr>
 *         <th>rejected</th>
 *         <td>If the message has been rejected <i>Note: this is not a guarantee that it will be logged if it is true.
 *         As handlers can reject messages on their own, if your handler is called before one that rejects the message,
 *         you will see it as not being rejected and any after it will see it as being rejected. Position matters in
 *         handlers</i></td>
 *     </tr>
 * </table>
 * @param handler {(data: object) => boolean | undefined} the handler
 */
export function addHandler(handler: (data: object) => boolean | undefined) {
    config.handlers.push(handler);
}

/**
 * Sets the minimum level that will be output. The outputs will include the given level
 * @param level {LogLevel} the minimum level to output
 */
export function setMinimumLevel(level: LogLevel) {
    config.minLevel = level;
}

/**
 * Sets the config value {@link config.colorsDisabled} which will skip all coloring functions
 */
export function disableColors() {
    config.colorsDisabled = true;
}

/**
 * Sets the config value {@link config.colorsDisabled} which will prevent skipping of coloring functions
 */
export function enableColors() {
    config.colorsDisabled = false;
}

/**
 * Sets the given key to the given value in the config. This should not technically be needed but is there for future
 * proofing. All config values should be able to be updated via {@link setMinimumLevel}, {@link setColorFunction},
 * {@link addHandler}, {@link disableColors} and {@link enableColors}
 * @param key {string} the key to update in the config (keys that descend levels should use dots to represent levels eg colors.INFO)
 * @param data {any} the value the key should be updated to
 */
export function configure(key: string, data: any) {
    let active = config;
    const segments = key.split('.');
    const last = segments[segments.length - 1];

    for (let i = 0; i < segments.length - 1; i++) {
        const element = segments[i];
        if (active.hasOwnProperty(element)) active = active[element];
        else throw new Error('Invalid config key');
    }

    active[last] = data;
}

/**
 * Pads the given string to the given length using the provided fill value (defaults to '0'). If the
 * given date is longer than or equal to the length it will be returned unchanged
 * @param data {string} the data to pad
 * @param length {number} the minimum length of the string
 * @param [fill='0'] {string} the character to fill the string with
 */
function padString(data: string, length: number, fill: string = '0') {
    if (data.length >= length) return data;

    let dataClone = data;
    while (dataClone.length < length) dataClone += fill;

    return dataClone;
}

/**
 * Pads the given number to the given length using {@link padString}
 * @param data {number} the data to pad
 * @param length {number} the minimum length of the string
 * @param [fill='0'] {string} the character to fill the string with
 */
function padNumber(data: number, length: number, fill: string = '0') {
    return padString(String(data), length, fill);
}

/**
 * Returns the current date and time (as fetched from a new Date instance) in the form
 * YYYY-MM-DD hh:mm:ss
 */
function getFormattedDateTime(): string {
    const date: Date = new Date();
    const year: string = padNumber(date.getFullYear(), 4);
    const month: string = padNumber(date.getMonth() + 1, 2);
    const day: string = padNumber(date.getDate(), 2);

    const hour: string = padNumber(date.getHours(), 2);
    const minute: string = padNumber(date.getMinutes(), 2);
    const second: string = padNumber(date.getSeconds(), 2);

    return `${year}-${month}-${day} ${hour}:${minute}:${second}`;
}

/**
 * Extracts the caller of the given depth from a new error stack. An error will be thrown if the
 * stack cannot be fetched (returns an undefined value), if there are not enough stack elements
 * to fetch the given depth or if the stack string is not parsable.
 * @param [depth=3] {number} the depth in the stack to fetch
 */
function extractCallerFromError(depth: number = 3): { method: string, file: string, line: string, character: string, context: string, stack: string } {
    const stack = new Error().stack;
    if (stack === undefined) throw new Error('failed to extract stack from a new error instance');

    let stackLines = stack.split('\n').slice(1);

    // Preprocess the stack lines
    stackLines = stackLines.map(line => line.trim());

    // Extract the caller for the given depth
    if (stackLines.length <= depth) throw new Error('call stack is too shallow to log');
    const target = stackLines[depth];

    let extractor = /^at (.+?) \((.+?):([0-9]+):([0-9]+)\)$/g;
    let elements = extractor.exec(target);

    let method;
    let file;
    let line;
    let character;

    if (elements === null) {
        extractor = /^at (.+?):([0-9]+):([0-9]+)$/g;
        elements = extractor.exec(target);

        if (elements === null) throw new Error('target element does not match a valid stack string: ' + target);

        method = '<no method>';
        file = elements[1];
        line = elements[2];
        character = elements[3];
    } else {
        method = elements[1];
        file = elements[2];
        line = elements[3];
        character = elements[4];
    }

    const pathSegments = file.split(sep);
    let context = pathSegments[0];

    if (pathSegments.length >= 2) {
        context = pathSegments.slice(pathSegments.length - 2, pathSegments.length).join(sep);
    }

    return {
        method,
        file,
        line,
        character,
        context,
        stack: target,
    };
}

/**
 * The function which logs the value to console (if it is allowed). It will get the log info from
 * {@link extractCallerFromError} and {@link getFormattedDateTime}. All handlers will be called with the details of the
 * log. If any return false or the log level is too low (beneath {@link config.minLevel} as set via
 * {@link setMinimumLevel}.
 * @param level {LogLevel} the level at which this message should be logged
 * @param message {string} the message to be logged
 * @param logFunction {Function} the function through which the value should be logged
 * @param color {(e: string) => string} the coloring function
 */
function output(level: LogLevel, message: string, logFunction: Function = console.log, color: Function = (e: string) => e) {
    let reject = levelMappings[level.toString()] < levelMappings[config.minLevel];

    const caller = extractCallerFromError();
    const time = getFormattedDateTime();

    if (config.hasOwnProperty('handlers')) {
        for (const handler of config.handlers) {
            if (typeof (handler) === 'function') {
                const result = handler({
                    time,
                    caller,
                    message,
                    color: config.colorsDisabled ? null : color,
                    level,
                    logFunction,
                    rejected: reject
                });

                if (result === false) reject = true;
            }
        }
    }

    if (reject) return;

    if (config.colorsDisabled) {
        logFunction(`[${time}][${caller.context}:${caller.line} ${caller.method}][${level}] ${message}`);
    } else {
        logFunction(color(`[${time}][${caller.context}:${caller.line} ${caller.method}][${level}] ${message}`));
    }
}

/**
 * Converts the given value to a string using {@link flattedStringify} but with exceptions for:
 * <ul>
 *     <li>undefined</li>
 *     <li>null</li>
 *     <li>Infinity / -Infinity</li>
 *     <li>NaN</li>
 *     <li>Error</li>
 * </ul>
 * @param value {any} the value to convert to string
 */
function stringify(value: any): string {
    let manualValues = [undefined, null, Infinity, -Infinity, NaN];

    for (let test of manualValues) if (value === test) return String(test);
    if (value === -0) return '-0';

    return util.inspect(value, true, null, true);
}

/**
 * Logs the message and data through {@link console.log} using the coloring function in {@link config} which can be
 * updated by {@link setColorFunction} via {@link output}
 * @param message the message to log the console
 * @param data the message to log alongside this message
 */
export function log(message: string, ...data: any[]) {
    output(LogLevel.BASE, message, console.log, config.colors.LOG);
    for (const value of data) {
        if (value === undefined || value === null) output(LogLevel.BASE, ` -> ${value}`, console.log, config.colors.LOG);
        else output(LogLevel.BASE, ` -> ${stringify(value)}`, console.log, config.colors.LOG);
    }
}

/**
 * Logs the message and data through {@link console.debug} using the coloring function in {@link config} which can be
 * updated by {@link setColorFunction} via {@link output}
 * @param message the message to log the console
 * @param data the message to log alongside this message
 */
export function trace(message: string, ...data: any[]) {
    output(LogLevel.TRACE, message, console.debug, config.colors.TRACE);
    for (const value of data) {
        if (value === undefined || value === null) output(LogLevel.TRACE, ` -> ${value}`, console.log, config.colors.TRACE);
        else output(LogLevel.TRACE, ` -> ${stringify(value)}`, console.log, config.colors.TRACE);
    }
}

/**
 * Logs the message and data through {@link console.debug} using the coloring function in {@link config} which can be
 * updated by {@link setColorFunction} via {@link output}
 * @param message the message to log the console
 * @param data the message to log alongside this message
 */
export function debug(message: string, ...data: any[]) {
    output(LogLevel.DEBUG, message, console.debug, config.colors.DEBUG);
    for (const value of data) {
        if (value === undefined || value === null) output(LogLevel.DEBUG, ` -> ${value}`, console.log, config.colors.DEBUG);
        else output(LogLevel.DEBUG, ` -> ${stringify(value)}`, console.log, config.colors.DEBUG);
    }
}

/**
 * Logs the message and data through {@link console.log} using the coloring function in {@link config} which can be
 * updated by {@link setColorFunction} via {@link output}
 * @param message the message to log the console
 * @param data the message to log alongside this message
 */
export function info(message: string, ...data: any[]) {
    output(LogLevel.INFO, message, console.log, config.colors.INFO);
    for (const value of data) {
        if (value === undefined || value === null) output(LogLevel.INFO, ` -> ${value}`, console.log, config.colors.INFO);
        else output(LogLevel.INFO, ` -> ${stringify(value)}`, console.log, config.colors.INFO);
    }
}

/**
 * Logs the message and data through {@link console.warn} using the coloring function in {@link config} which can be
 * updated by {@link setColorFunction} via {@link output}
 * @param message the message to log the console
 * @param data the message to log alongside this message
 */
export function warn(message: string, ...data: any[]) {
    output(LogLevel.WARN, message, console.warn, config.colors.WARN);
    for (const value of data) {
        if (value === undefined || value === null) output(LogLevel.WARN, ` -> ${value}`, console.warn, config.colors.WARN);
        else output(LogLevel.WARN, ` -> ${stringify(value)}`, console.warn, config.colors.WARN);
    }
}

/**
 * Logs the message and data through {@link console.error} using the coloring function in {@link config} which can be
 * updated by {@link setColorFunction} via {@link output}
 * @param message the message to log the console
 * @param data the message to log alongside this message
 */
export function error(message: string, ...data: any[]) {
    output(LogLevel.ERROR, message, console.error, config.colors.ERROR);
    for (const value of data) {
        if (value === undefined || value === null) output(LogLevel.ERROR, ` -> ${value}`, console.error, config.colors.ERROR);
        else output(LogLevel.ERROR, ` -> ${stringify(value)}`, console.error, config.colors.ERROR);
    }
}

/**
 * Logs the message and data through {@link console.error} using the coloring function in {@link config} which can be
 * updated by {@link setColorFunction} via {@link output}
 * @param message the message to log the console
 * @param data the message to log alongside this message
 */
export function fatal(message: string, ...data: any[]) {
    output(LogLevel.FATAL, message, console.error, config.colors.FATAL);
    for (const value of data) {
        if (value === undefined || value === null) output(LogLevel.FATAL, ` -> ${value}`, console.error, config.colors.FATAL);
        else output(LogLevel.FATAL, ` -> ${stringify(value)}`, console.error, config.colors.FATAL);
    }
}

/**
 * A single object that contains all the logging functions, useful for passing around when required and allows importing
 * a single object
 */
export const Logs = {
    fatal,
    error,
    warn,
    info,
    debug,
    trace,
    log,
};
