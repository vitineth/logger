# logger

Minimal logging module for node written in typescript

## API

### Basics

Importing:

``` js
import * as _ from 'logger'; // TypeScript
const _ = require('logger'); // Node.js
```

### `log(string, ...any)`

### `trace(string, ...any)`

### `debug(string, ...any)`

### `info(string, ...any)`

### `warn(string, ...any)`

### `error(string, ...any)`

### `fatal(string, ...any)`

All logging functions have the same signature and function in the same way. The given string will be logged to the console in the format described at the base of this document. The additional parameters passed will be converted to JSON (with some special exceptions to allow for the stringification of `Error`, `Infinity/-Infinity`, `undefined`, `null`  and `NaN`) and logged after the message prefixed by `->` to mark additional data rather than their own log messages.

### `setColorFunction(LogLevel, (data: string) => string)`

Each log level is associated with a colouring function which applies ANSI color codes to the output ready for logging to the console. This function allows you to associate your own function with a given log level to customise the output. 

### `addHandler((data: object) => boolean | undefined)`

Handlers are log interceptors meaning that they will be notified before log messages are sent to the console. Handlers will receive an object as outlined below which contains all details used in sending the log to the console. Handlers have the option of returning a boolean value which can cancel the logging of a message by returning false (compared via strict equality [`===`] so returning undefined is not the same as false). An important note about handlers and cancelling messages are that handlers are executed in the order they are registered and earlier handlers will not be notified when a later handler cancels the logging so the `rejected` key is not the absolute truth whether a message is being logged.

Object passed to the handler function:

``` js
{
    time: string,
    caller: {
        method: string,
        file: string,
        line: string,
        character: string,
        context: string,
        stack: string[]
    },
    message: string,
    color: null | (m: string) => string,
    level: LogLevel,
    logFunction: console.log|warn|error|debug,
    rejected: boolean
}
```

Updating these values will not change the log message.

### `setMinimumLevel(LogLevel)`

Sets the minimum log level that will be output to the console

### `disableColors()`

Remove all colouring from future log messages. Handlers will begin to receive `null` for the `color` attribute.

### `enableColors()`

Enable colouring for future log messages. Handlers will begin to receive actual functions for the `color` attribute

### `configure(string, any)`

Manually sets the given key to the given value in the config. This should not technically be needed but is there for future proofing. All config values should be able to be updated via `setMinimumLevel}`, `setColorFunction`, `addHandler`, `disableColors` and `enableColors`
### class `LogLevel`

An enum representing all available log levels:

```js
enum LogLevel {
    BASE = 'BASE',
    TRACE = 'TRACE',
    DEBUG = 'DEBUG',
    INFO = 'INFO',
    WARN = 'WARN',
    ERROR = 'ERROR',
    FATAL = 'FATAL',
}
```

`BASE` is the internal representation for the default `log` function. You are expected to use more specific levels as `BASE` is weighted less than `TRACE`.

## Log Format

`[YYYY-MM-DD HH:mm:ss][<file>:<line> <method>][<level>] <message>`

The line will be colored according to the colouring function in the config which can be set via `setColorFunction`. The defaults are:

```js
BASE = unchanged
LOG = unchanged
TRACE = green
DEBUG = magenta
INFO = unchanged
WARN = yellow
ERROR = red
FATAL = red background
```

The format cannot currently be changed